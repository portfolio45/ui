module.exports = {
	themeConfig: {
		sidebar: [
			{
				title: 'How this blog was built',
				children: [
					[
						'/articles/bootstrap_vuepress.md',
						'Step 1. VuePress bootstrap',
					],
				],
			},
		],
	},
}
