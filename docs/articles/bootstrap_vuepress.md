## Step 1. Bootstrap VuePress

### Prerequirements:

1. [NodeJS](https://nodejs.org/en/) with `npm` installed
2. [GitLab](https://gitlab.com/) acccout. We are going to use it as a remote repository for our code and it CI/CD pipelines to deploy our blog in the future.
3. [Git](https://git-scm.com/)
4. In this articles Im going to use [VS Code](https://code.visualstudio.com/) as my IDE. You could use any of them but some of the plugins could be unavailable.

### Bootstrap VuePress

#### Create and init the project directory

```sh
$ mkdir vue-blog
$ cd vue-blog
$ npm init -y
```

#### Bootstrap an VuePress project

```sh
$ npm install -D vuepress
```

#### Add next two lines to `script` section of `package.json` file

```json
{
	"scripts": {
		"docs:dev": "vuepress dev docs",
		"docs:build": "vuepress build docs"
	}
}
```

#### Create a `docs` directory with `README.md` file inside

```sh
$ mkdir docs && echo '# Hello VuePress' > docs/README.md
```

VuePress takes `docs` dir as it initial directory to build a Vue site from. As for `README.md` file int the root of the `docs` - it will become an `/index.html` point on our site. This is a recommended folders structure for VuePress projects. But it could be changed thru configuration file.

#### Build the project

```sh
$ npm run docs:dev
```

VuePress will start a hot-reloading development server at http://localhost:8080
